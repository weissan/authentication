/*******************************************************
File name : gen_key.h
Description : 
  
Version: 2.0.0
Create Date : 2015-02-15 17:01
Modified Date : 2015-02-15 17:01
Revision : none
  
Author : 罗伟 (luowei@e.hunantv.com)
Company: 芒果TV 2015 版权所有
  
Please keep this mark, tks!
******************************************************/
 
#ifndef __gen_key_h__
#define __gen_key_h__
 
#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif 

#ifdef __cplusplus
extern "C" 
{
#endif

/*
 *  /@ param uri: 文件URI
 *  /@ parm crypt_key: 加密秘钥
 *  /@ url_valid_time: 加密串有效时间，单位为秒
 *	/@ res_kye: 返回key串(32位)
 *
 *	return 大于0：成功.
 *
 *	example:
 *	uri: /mp4/2014/dongman/nks_4353/000004E6F3DD5D3D790C2973D562DE4E_20140405_1_1_811.mp4
 *	crypt_key: 12345678
 *	url_valid_time: 86400
 *
 *	gen_default_key(uri, crypt_key, url_valid_time, res);
 *
 *
 */
int 
gen_default_key(const char* uri, 
		const char* crypt_key, 
		int url_valid_time, 
		char*  res_key);


/*
 *  /@ param uri: 文件URI
 *  /@ parm p2p_key: 加密秘钥
 *  /@ url_valid_time: 加密串有效时间
 *	/@ res_kye: 返回key串
 *
 *	return 大于0：成功.
 *
 *	example:
 *	uri: /mp4/2014/dongman/nks_4353/000004E6F3DD5D3D790C2973D562DE4E_20140405_1_1_811.mp4
 *	p2p_key: $^34*678
 *	url_valid_time: 86400
 *
 *	gen_p2p_key(uri, crypt_key, url_valid_time, res);
 *
 */
int 
gen_p2p_key(const char* uri, 
		const char* p2p_key, 
		int url_valid_time, 
		char*  res_key);

/*
 *  /@ param uri: 文件URI
 *  /@ parm vip_key: 加密秘钥
 *  /@ parm uuid: 会员ID， HNTV00001参数携带
 *  /@ url_valid_time: 加密串有效时间
 *	/@ res_kye: 返回key串
 *
 *	return 大于0：成功.
 *
 *	example:
 *	uri: /mp4/2014/dongman/nks_4353/000004E6F3DD5D3D790C2973D562DE4E_20140405_1_1_811.mp4
 *	vip_key: &^34*67!
 *	url_valid_time: 600
 *
 *	gen_vip_key(uri, crypt_key, url_valid_time, res);
 *
 */
int 
gen_vip_key(const char* uri, 
		const char* vip_key, 
		const char* uuid,
		int url_valid_time, 
		char*  res_key);

/*
 * 
 * /@ param uri			播放视频URI
 * /@ param key			用于校验签名的秘钥
 *
 * @return 0: 成功， 其它： 失败
 */
int 
verify_url(const char* url, const char* key);
 
#ifdef __cplusplus
}  /* end extern "C" */
#endif

#endif // __gen_key.h__

