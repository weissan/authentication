#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <assert.h>
#include <ctype.h>

//curl指定静态链接
#define CURL_STATICLIB  //HTTP_ONLY

#include <curl/curl.h>

#include "gen_key.h"
#include "md5.h"
#include "url.h"
//#include <syslog.h>

#define MALLOC(n, size) calloc(n, size)
//  syslog(LOG_MAKEPRI(LOG_USER, LOG_ERR),"ERROR: %s(%s,%s) -- %s:%d\n", __func__, url, key, __FILE__, __LINE__);


char* 
gen_md5(char* data) 
{
  unsigned char digest[16];
  char* md5_string = (char *)MALLOC(1,34);
  int i;
  md5_state_t md5;

  md5_init(&md5);
  md5_append(&md5, (unsigned char*)data, strlen(data));
  md5_finish(&md5, digest);

  for(i = 0; i < 16; i++){
    sprintf(md5_string+2*i, "%02x", digest[i]);
  }
  
  return md5_string;
}

int
strsplit_once (const char *str, char *parts[], const char *delimiter) {
  int i = 0;
  char *tmp = strdup(str);
  char *p = tmp;
  char *key_point=NULL;

  while(p) {
    while ( (key_point = strsep(&p,delimiter))) {
      if (0 == *key_point) continue;
      else {
        parts[i++] = strdup(key_point);
        if(p) {
          parts[i++] = strdup(p);
          p = NULL;
        }
        break;
      }
    }
  }

  free(tmp);

  return i;
}

int
strsplit (const char *str, char *parts[], const char *delimiter) {
  int i = 0;
  char *tmp = strdup(str);
  char *p = tmp;
  char *key_point=NULL;

  while(p) {
    while ( (key_point = strsep(&p,delimiter))) {
      if (0 == *key_point) continue;
      else parts[i++] = strdup(key_point);
    }
  }

  free(tmp);

  return i;
}

//返回值大于0 OK
static int 
parse_uri(const char* uri, char** fmt)
{
  enum{NUM = 10};
  int i, j, cnt, elm_cnt, index = 0;
  char **parts = NULL;
  char **elms= NULL;
  int ret = 0;

  //URI必须以'/'打头
  if (uri[0] != '/')
  {
    return -__LINE__;
  }

  elms = MALLOC(NUM, sizeof(char *));
  parts = MALLOC(NUM, sizeof(char *));
  cnt = strsplit(uri, parts, "/"); 
  for(i=0; i<cnt; ++i)
  {
    elm_cnt = strsplit(parts[i], elms, "_");
    if (elm_cnt < 1)
    {
      ret =  -__LINE__;
      goto PARSE_URI_FREE;
    }

    for (j=0; j<elm_cnt; ++j)
    {
      if (index < NUM ) 
      {
        strcpy(fmt[index++], elms[j]);
      }

      free(elms[j]);
      elms[j] = NULL;
    }
  }

PARSE_URI_FREE:
  for(i=0;i<NUM; ++i)
  {
    free(parts[i]);
    free(elms[i]);
  }
  free(parts);
  free(elms);

  return ret<0?ret:index;
}

static int 
parse_query(const char* query, char** fmt)
{
  enum{NUM = 100};
  int i, j, cnt, elm_cnt, index = 0;
  char **parts = MALLOC(NUM, sizeof(char *));
  char **elms= MALLOC(NUM, sizeof(char *));
  int ret = 0;
  cnt = strsplit(query, parts, "&"); 
  if (cnt > 40)
  {
    ret =  -__LINE__;
    goto PARSE_QUERY_FREE;
  }

  for(i=0; i<cnt; ++i)
  {
    //if(!strncmp(parts[i],"payload=",8))
    //{
    elm_cnt = strsplit_once(parts[i], elms, "=");
    //}
    //else 
    //  elm_cnt = strsplit(parts[i], elms, "=");

    if (elm_cnt != 2)
    {

      elm_cnt = 0;
    }
  
    for(j=0; j<elm_cnt; ++j)
    {
      if (index < NUM ) 
      {
        strcpy(fmt[index++], elms[j]);
      }

      free(elms[j]);
      elms[j] = NULL;
    }
  }

PARSE_QUERY_FREE:
  for(i=0;i<NUM; ++i)
  {
    free(parts[i]);
    free(elms[i]);
  }
  free(parts);
  free(elms);

  return ret<0?ret:index;
}


int 
gen_default_key(const char* uri, 
    const char* crypt_key, 
    int url_valid_time, 
    char*  res_key)
{
  enum{NUM=11};
  enum{BUF_SIZE=1024};
  int integer_num = 0;//, float_num = 0;
  int steps[3] = {0};
  long cur_time = time(NULL);
  char* fmt[NUM] = {NULL};
  int index = 0;
  char crypt_str[BUF_SIZE];
  int ret = 0;
  char* tmp=NULL;

  //生成加密秘钥
  integer_num = cur_time / url_valid_time;
  //float_num = cur_time % url_valid_time;
  steps[0] = integer_num;
  steps[1] = integer_num - 1;
  steps[2] = integer_num + 1;

  //初始化
  for (index=0; index<NUM; ++index)
  {
    fmt[index] = (char*)MALLOC(BUF_SIZE, sizeof(char));
    bzero(fmt[index], BUF_SIZE);
  }

  //解析uri，提取参数
  //fmt["factory"] =  factory;
  //fmt["year"] =  year;
  //fmt["cid"]  = cid;
  //fmt["vname"] =  vname;
  //fmt["vid"]  = vid;
  //fmt["fmd5"] =  fmd5;
  //fmt["yyd"]  = yyd;
  //fmt["ext"]  = ext;
  ret = parse_uri(uri, fmt);
  if (ret <=0 )
  {
    strcpy(res_key, "wrong format URI.");
    for (index=0; index<NUM; ++index)
    {
      free(fmt[index]);
    }

    return ret;
  }

  snprintf(crypt_str, BUF_SIZE, "%d%s%s", steps[0], crypt_key, fmt[5]);
  strcpy(res_key, (tmp = gen_md5(crypt_str)));
  free(tmp);

  for (index=0; index<NUM; ++index)
  {
    free(fmt[index]);
  }

  return ret;
}

/*
 *  /@ param uri: 文件URI
 *  /@ parm crypt_key: 加密秘钥
 *  /@ url_valid_time: 加密串有效时间
 *  /@ res_kye: 返回key串
 *
 *  return 0：成功， 1: 失败
 */
int 
gen_p2p_key(const char* uri, 
    const char* crypt_key, 
    int url_valid_time, 
    char*  res_key)
{
  return gen_default_key(uri, crypt_key, 
      url_valid_time, res_key);
}

int 
gen_vip_key(const char* uri, 
    const char* crypt_key, 
    const char* uuid,
    int url_valid_time, 
    char*  res_key)
{
  enum{NUM=11};
  enum{BUF_SIZE=1024};
  int integer_num = 0;//, float_num = 0;
  int steps[3] = {0};
  long cur_time = time(NULL);
  char* fmt[NUM] = {NULL};
  int index = 0;
  char crypt_str[BUF_SIZE];
  int ret = 0;
  char* tmp = NULL;

  //生成加密秘钥
  integer_num = cur_time / url_valid_time;
  //float_num = cur_time % url_valid_time;
  steps[0] = integer_num;
  steps[1] = integer_num - 1;
  steps[2] = integer_num + 1;

  //初始化
  for (index=0; index<NUM; ++index)
  {
    fmt[index] = (char*)MALLOC(BUF_SIZE, sizeof(char));
    bzero(fmt[index], BUF_SIZE);
  }

  //解析uri，提取参数
  //fmt["factory"] =  factory;
  //fmt["year"] =  year;
  //fmt["cid"]  = cid;
  //fmt["vname"] =  vname;
  //fmt["vid"]  = vid;
  //fmt["fmd5"] =  fmd5;
  //fmt["yyd"]  = yyd;
  //fmt["ext"]  = ext;

  ret = parse_uri(uri, fmt);
  if (ret <=0 )
  {
    for (index=0; index<NUM; ++index)
    {
      free(fmt[index]);
    }

    strcpy(res_key, "wrong format URI");
    return ret;
  }

  snprintf(crypt_str, BUF_SIZE, "%s%s%s%d%s", 
      fmt[2], uuid, fmt[4], 
      steps[0], crypt_key);

  //printf("vip v gen: \n");
    strcpy(res_key, (tmp = gen_md5(crypt_str)));
    free(tmp);
  //printf(" v: %d\n cid: %s\n uid:%s\n vid:%s\n key:%s\n t: %s\n", 
  //    steps[0], fmt[2], uuid, fmt[4], crypt_key, res_key);

  for (index=0; index<NUM; ++index)
  {
    free(fmt[index]);
  }
  return ret;
}

/*
 * 
 * /@ param sign:    客户端携带的签名
 * /@ param sign_key    用于校验签名的秘钥
 * /@ param uri      播放视频URI
 * /@ param fid      播放视频FID
 * /@ param now      鉴权时间
 * /@ param uuid    客户唯一ID
 * /@ param win      播放串有效时间，单位为秒
 * /@ param rdur    会话保持时长  
 * /@ param srgid    服务地区ID
 * /@ param urgid    用户地区ID
 * /@ param srgids    服务机房ID
 * /@ param limitrate  限速值
 * /@ param arange    免费播放前n秒
 * /@ param payload    冗余播放参数
 * /@ param nid      节点ID
 *
 * @return 0: 成功， 其它： 失败
 */
static int 
verify_sign (const char* sign,
    const char* sign_key,
    const char* uri,
    const char* fid, 
    const char* uuid,
    const char* now,
    int srgid,
    int urgid,
    int arange,
    int nid,
    int limitrate,
    int win,
    int pno,
    const char* rdur,
    const char* payload,
    const char* srgids,
    const char* ver)
{
  enum{BUF_LEN=4096};
  char* buf = calloc(BUF_LEN, sizeof(char));
  int ret = 0;
  char* tmp = NULL;
  int outlen = 0;
  int i = 0;

  if (NULL == sign || NULL == sign_key
      || NULL == uri || NULL == fid
      || NULL == uuid || NULL == now
      || NULL == rdur || NULL == payload
      || NULL == srgids || NULL == ver)
  {
    free(buf);
    return -__LINE__;
  }

  //对payload做urldecode
  if(NULL != payload)
  {
    CURL *curl = curl_easy_init();
    if(curl) 
    {
      //char *output = curl_easy_escape(curl, payload, strlen(payload));
      char *output = curl_easy_unescape(curl, payload, strlen(payload), &outlen);
      if(output) 
      {
        tmp = strdup(output);
        curl_free(output);
        //printf("payload: %s\n", tmp);
      }
      curl_easy_cleanup(curl);
    }
  }

  snprintf(buf, BUF_LEN, "%s%s%s"
      "%s%s%d%s"
      "%d%d%s"
      "%d%d%s%d%s%d",
      sign_key, uri, fid,
      now, uuid, win, rdur, 
      srgid, urgid, srgids,
      limitrate, arange, tmp, nid, ver, pno);

  free(tmp);

  for(i=0;i<strlen(buf);++i)
  {
    buf[i] = tolower(buf[i]);
  }

  //printf("sign str: %s, md5:%s\n", buf,gen_md5(buf));
  ret = (strcmp(sign, (tmp = gen_md5(buf))) == 0) ? 0: -__LINE__;
  //printf("CCCCCCCCCCC %d = strcmp(%s, %s)", ret, sign, gen_md5(buf));
  //printf("sign: %s\n", gen_md5(buf));
  free(tmp);
  free(buf);

  return ret;
}

int verify_url(const char* url, const char* key)
{
  enum{NUM=100};
  enum{BUF_SIZE=1024};
  struct parsed_url* URL = NULL;
  char* sign = NULL;
  char* fid = NULL;
  char uri[BUF_SIZE];
  char* uuid = NULL;
  char* rdur = "0"; 
  char* payload = NULL;
  char* srgids = NULL;
  char* ver = NULL;
  char* now=NULL;
  long srgid=0, urgid=0, arange=0, nid=0, limitrate=0, win=30, pno=0;
  int ret = -1, index=0, cnt = 0, i=0;
  char* fmt[NUM] = {NULL};

  //判断url长度是否在规定范围内
  if(strlen(url) > 1024){
    return -__LINE__;
  }

  //初始化
  for (index=0; index<NUM; ++index)
  {
    fmt[index] = (char*)MALLOC(BUF_SIZE, sizeof(char));
    bzero(fmt[index], BUF_SIZE);
  }
  

  URL = parse_url(url);

  if ((NULL == URL) || (NULL == URL->path) || (NULL == URL->query) || 0 == strlen(URL->query))
  {

    //printf("parse_Url error, url: %s\n", url);
    parsed_url_free(URL);
    for (index=0; index<NUM; ++index)
    {
      free(fmt[index]);
    }
    //play url解析失败
    return -__LINE__;
  }
  uri[0] = '/';
  strcpy(uri+1, URL->path);

  cnt = parse_query(URL->query, fmt);
  //参数解析失败
  if (cnt <=0 )
  {
    //printf("parse_query error, query: %s\n", URL->query);
    parsed_url_free(URL);
    for (index=0; index<NUM; ++index)
    {
      free(fmt[index]);
    }
    return cnt;
  }
  
  for(i =0;i<cnt; i+=2)
  {
    if (0 == strcmp("uuid",fmt[i]))
    {
      uuid = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("sign", fmt[i]))
    {
      sign = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("rdur", fmt[i]))
    {
      rdur = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("fid", fmt[i]))
    {
      fid = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("payload", fmt[i]))
    {
      payload = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("srgids", fmt[i]))
    {
      srgids = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("ver", fmt[i]))
    {
      ver= fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("t", fmt[i]))
    {
      now = fmt[i+1];
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("srgid", fmt[i]))
    {
      srgid = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("urgid", fmt[i]))
    {
      urgid = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("arange", fmt[i]))
    {
      arange = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("nid", fmt[i]))
    {
      nid = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("limitrate", fmt[i]))
    {
      limitrate = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("win", fmt[i]))
    {
      win = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else if (0 == strcmp("pno", fmt[i]))
    {
      pno = atoi(fmt[i+1]);
      //printf("%s=%s\n", fmt[i], fmt[i+1]);
    }
    else
    {
      //printf("extra param, %s:%s\n", fmt[i], fmt[i+1]);
    }
  }

  ret = verify_sign (sign, key,
    uri,
    fid,
    uuid,
    now,
    srgid,
    urgid,
    arange,
    nid,
    limitrate,
    win,
    pno,
    rdur,
    payload,
    srgids,
    ver
    );
  parsed_url_free(URL);

  for (index=0; index<NUM; ++index)
  {
    free(fmt[index]);
  }

  return ret;
}

