
/*******************************************************
  File name : url.h
Description : 

Version: 2.0.0
Create Date : 2015-03-16 14:58
Modified Date : 2015-03-16 14:58
Revision : none

Author : 刘小刚(liuxiaogang) liuxiaogang@e.hunantv.com
Company: 芒果TV 2015 版权所有

Please keep this mark, tks!
 ******************************************************/

#ifndef __url_h__
#define __url_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif 

/*
 *  * URL storage
 *   */
struct parsed_url {
	char *scheme;               /* mandatory */
	char *host;                 /* mandatory */
	char *port;                 /* optional */
	char *path;                 /* optional */
	char *query;                /* optional */
	char *fragment;             /* optional */
	char *username;             /* optional */
	char *password;             /* optional */
};

#ifdef __cplusplus
extern "C" {
#endif

/*
 *      * Declaration of function prototypes
 *           */
struct parsed_url * parse_url(const char *);
void parsed_url_free(struct parsed_url *);

#ifdef __cplusplus
}
#endif


#endif // __url.h__


